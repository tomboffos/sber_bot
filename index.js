const express = require('express')
const TelegramBot = require('node-telegram-bot-api');
const axios = require('axios');
const app = express()
const port = 3000

const token = '2145840647:AAEUEjgmAT9RsEiqNpdjMP5Z466Omlc2CLo'
const bot = new TelegramBot(token, {polling: true});


const prices = [
    5000, 10000, 20000, 30000, 40000, 50000, 100000
]

bot.onText(/\/echo (.+)/, (msg, match) => {
    // 'msg' is the received Message from Telegram
    // 'match' is the result of executing the regexp above on the text content
    // of the message

    const chatId = msg.chat.id;
    const resp = match[1]; // the captured "whatever"

    // send back the matched "whatever" to the chat
    bot.sendMessage(chatId, resp);
});

app.use

bot.on('message', (msg) => {
    if (msg.text.split(' ')[0] === 'Код')
        fetchCurrenciesByApi(msg.text.split(' ')[2].replace(',', ''), bot, msg, msg.text.split(' ')[4]).then(r => {
        })
    else if (msg.text.split(' ')[0] === 'Валюта:')
        createOrder(msg.text, bot, msg).then(r => {
        })
    else
        switch (msg.text) {
            case '/start' :
                console.log(msg.chat.id)
                bot.sendMessage(msg.chat.id, 'Добро пожаловать', {
                    "reply_markup": {
                        "keyboard": [["Новая покупка", "Мой профиль"], ["Тех.поддержка", "Калькулятор"], ["Инструкция по использованию кода"]]

                    }
                })
                break;

            case 'Отменить':
                bot.sendMessage(msg.chat.id, 'Добро пожаловать', {
                    "reply_markup": {
                        "keyboard": [["Новая покупка", "Мой профиль"], ["Тех.поддержка", "Калькулятор"], ["Инструкция по использованию кода"]]

                    }
                })
                break;

            case 'Калькулятор':
                sendCourses(msg)
                break;
            case 'Новая покупка':
                getProducts(msg)
                break;
            case 'Инструкция по использованию кода':
                bot.sendMessage(msg.chat.id, 'https://telegra.ph/Instrukciya-07-30-2')
                break;

            case 'Тех.поддержка':
                bot.sendMessage(msg.chat.id, 'По всем вопросам пишите в телеграм нашему оператору: @KSB_support_bot')
                break;

            case 'Мой профиль':
                bot.sendMessage(msg.chat.id, `ID: ${msg.from.id} \nЗавершенных покупок: 0`)
                break;

            case 'Отменить заказ':
                bot.sendMessage(msg.chat.id, 'Ваш заказ отменен. Очень жаль :(. Будем рады видеть вас в следующий раз!', {
                    "reply_markup": {
                        "keyboard": [["Новая покупка", "Мой профиль"], ["Тех.поддержка", "Калькулятор"], ["Инструкция по использованию кода"]]

                    }
                })
                break;
            case 'Я оплатил':
                bot.sendMessage(msg.chat.id, 'Спасибо за оплату!\n' +
                    'Мы отправим Вам Ваш заказ, как только получим первые подтверждения от блокчейна. \n' +
                    '(обычно 2-10 мин)', {
                    "reply_markup": {
                        "keyboard": [["Новая покупка", "Мой профиль"], ["Тех.поддержка", "Калькулятор"], ["Инструкция по использованию кода"]]

                    }
                })
                break;
            default:
                break;
        }

});

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.get('/order/declined', (req, res) => {
    bot.sendMessage(req.query.chat_id, `Ваш заказ номер ${req.query.id} отменен `)
    res.send(200)
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})

async function sendCourses(msg) {
    const response = await axios.get('https://api1.binance.com/api/v3/ticker/price');
    const responseProducts = await axios.get('https://superbot.ltd/api/products')

    let multiCoef = response.data.filter((e) => e.symbol === 'USDTRUB')[0].price
    let prices = response.data.filter(item => item.symbol.replace('USDT', '') === 'TRX' || item.symbol.replace('USDT', '') === 'LTC' || item.symbol.replace('USDT', '') === 'BTC'
        || item.symbol.replace('USDT', '') === 'XMR' || item.symbol.replace('USDT', '') === 'DASH' || item.symbol.replace('USDT', '') === 'BCH')
        .map((item) => [
            (1 / item.price) / multiCoef,
            item.symbol.replace('USDT', '')
        ])


    console.log(prices)
    console.log(responseProducts.data.data)


    let calculatedPricesMessages = responseProducts.data.data.map((price) => [`${price.name}\n\n${prices.map((currency) => `${(currency[0] * price.price).toFixed(5)} ${currency[1]}`).join('\n')}`])
    bot.sendMessage(msg.chat.id, calculatedPricesMessages.join('\n\n\n'))
}

let chosenPrice = null;
let secondPrice = null;

async function fetchCurrencies(price, bot, msg, productId) {
    const garantexResponse = await axios.get(`https://garantex.io/api/v2/trades?market=ethusdt`)


    const response = await axios.get('https://api1.binance.com/api/v3/ticker/price');
    chosenPrice = productId;
    let multiCoef = response.data.filter((e) => e.symbol === 'USDTRUB')[0].price
    let prices = response.data.filter(item => item.symbol.replace('USDT', '') === 'TRX' || item.symbol.replace('USDT', '') === 'LTC'
        || item.symbol.replace('USDT', '') === 'XMR' || item.symbol.replace('USDT', '') === 'DASH' || item.symbol.replace('USDT', '') === 'BCH')
        .map((item) => [`Валюта: ${price * (1 / item.price) / multiCoef} ${item.symbol.replace('USDT', '')}`])
    let resultRub = ((1 / garantexResponse.data[0].price) / multiCoef) * price

    const blockChainResponse = await axios.get(`https://blockchain.info/ticker`)
    let resultMain = ((1 / blockChainResponse.data.USD['15m']) / multiCoef) * price

    prices.push([`Валюта: ${resultRub} ETH`])

    prices.push([`Валюта: ${resultMain} BTC`])
    prices.push(['Отменить'])
    bot.sendMessage(msg.chat.id, 'Выберите валюту', {
        "reply_markup": {
            "keyboard": prices
        }
    })

    console.log(chosenPrice)

}


async function fetchCurrenciesByApi(price, bot, msg, productId) {
    const response = await axios.get('https://superbot.ltd/api/currency')

    const secondResponse = await axios.get('https://api1.binance.com/api/v3/ticker/price');

    chosenPrice = productId;

    let prices = []

    let multiCoef = secondResponse.data.filter((e) => e.symbol == 'USDTRUB')[0].price
    console.log(response.data)
    await response.data.map(async (currency, index) => {
        if (currency.source_id == 1)
            secondResponse.data.filter(item => item.symbol.replace('USDT', '') == currency.currency_code)
                .forEach((item) => {
                    let secondPrice = currency.currency_fee != 0 ? (((1 / item.price) / multiCoef) * price) - (((1 / item.price) / multiCoef) * price * (1 / currency.currency_fee)) : ((1 / item.price) / multiCoef) * price
                    prices.push([`Валюта: ${secondPrice.toFixed(currency.round_point)} ${item.symbol.replace('USDT', '')}`])

                })
        if (currency.source_id == 3) {
            let mainResponse = await axios.get(`https://garantex.io/api/v2/trades?market=ethusdt`)
            let resultRub = currency.currency_fee != 0 ? (((1 / mainResponse.data[0].price) / multiCoef) * price) - (((1 / mainResponse.data[0].price) / multiCoef) * price * (1 / currency.currency_fee)) : ((1 / mainResponse.data[0].price) / multiCoef) * price
            await prices.push([`Валюта: ${resultRub.toFixed(currency.round_point)} ETH`])
        }
        if (currency.source_id == 2) {
            let mainResponse = await axios.get(`https://blockchain.info/ticker`)
            console.log(mainResponse)
            let resultRub = currency.currency_fee != 0 ? (((1 / mainResponse.data.USD['15m']) / multiCoef) * price) - (((1 / mainResponse.data.USD['15m']) / multiCoef) * price * (1 / currency.currency_fee/100)) : ((1 / mainResponse.data.USD['15m']) / multiCoef) * price
            console.log('MAIN'+currency.currency_fee)
            await prices.push([`Валюта: ${resultRub.toFixed(currency.round_point)} BTC`])
        }
        console.log(prices.length)
        console.log(index)
        if (prices.length == index + 1) {
            prices.push(['Отменить'])

            bot.sendMessage(msg.chat.id, 'Выберите валюту', {
                "reply_markup": {
                    "keyboard": prices
                }
            })
        }
    })


}


async function getProducts(msg) {
    axios.get('https://superbot.ltd/api/products').then(response => {
        let keyArray = response.data.data.map((product) => {
            return [`Код на ${product.price} Руб ${product.id}`]
        })
        keyArray.push(['Отменить'])
        bot.sendMessage(msg.chat.id, 'Отлично выберите что хотите купить', {
            "reply_markup": {
                "keyboard": keyArray
            }
        })
    })

}

async function createOrder(code, bot, msg) {
    try {
        const response = await axios.post(`https://superbot.ltd/api/order/create/${chosenPrice}`, {
            'total': `${code.split(' ')[1]} ${code.split(' ')[2]}`,
            'chat_id': msg.chat.id
        })
        console.log(response.data)
        bot.sendMessage(msg.chat.id, `Ура!
Создан новый заказ!
ID: ${response.data.id}-${Math.random()*2020}
        
        
⏳На обмен есть 60 минут.  По истечению этого времени обмен будет автоматически отменен. Отправляйте указанную сумму в точности, как указано. Иначе транзакция не будет зачислена ботом.
        
Адрес: ${response.data.data.address} 
Сумма: ${code.split(' ')[1]} ${code.split(' ')[2]}`, {
            "reply_markup": {
                "keyboard": [['Я оплатил', 'Отменить заказ']]
            }
        })
    } catch (e) {
        console.log(e)
    }
}
